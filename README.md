# woocommerce-101
> PHP 综合能力基础题目

## tasks
> 具体任务

- 0: 完成部署
- 1: 发布文档
- 2: 开发并部署插件


>> 基础要求:

自行探索并完成部署, 要求:

- 0: Wordpress 最新版本
- 1: MultiSite 子域名模式
- 2: 发布3个子站, 
    + 1个 blog
    + 2个 WooCommerce
- 3: 3站不同皮肤
- 4: WooCommerce 基本可用
    + 有商品
    + `可支付`

给出完备的部署手册,并发布为 Blog 子站文章


>> 进阶要求:

并开发插件原型(完成越多越好):

- 0: 可以通过插件安装渠道标准安装
- 1: 在 WooComerce 管理后台插入一个菜单
    + 在定单菜单之前插入
    + 可以跳转到 blog 子站
    + 并自动进入部署手册文章页
- 2: 在 WooCommerce 管理后台插入一个菜单
    + 在定单菜单之前插入
    + 名为:`RESTful Key`
    + 点击后自动输出一则JSON:
        * 包含当前 WooComerce 门店 RESTful 接口必要数据:
            - Consumer Key 
            - Consumer Secret
- 3: 在 WooCommerce 子店页面底部自动插入一个订阅入口
    + 收集用户邮箱
    + 返回订阅成功说明页面
    + 有合理数据库表收集**有效**订阅邮箱
    + 能导出 .csv 数据文件
    + (可选)能合理批量发送促销邮件

### 加分项

- 发布在 Aliyun , 并合理允许访问
- 如果心有余力, 最好同时提供 英文版本的部署手册;

参考:
[How To Install WordPress with LEMP on Ubuntu 18.04 | DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lemp-on-ubuntu-18-04)

## 提交

- 请 fork 仓库
- 追加包含你姓名的成果说明文件:
    + .md 格式
    + 说明你交付网站链接
    + 各阶段用时
- 然后, 提交 PR
- 最后, 在对应面试群中, 给出 PR 链接
    + 以及对应系统后台管理员帐号/口令

## 姓名

谭维全

## 任务完成说明

- 实现WordPress子域名多站点部署用时约8小时，包括主题选择，支付实现探索，发布文档，支付由于需要账号，没有合适的账号所以无法实现测试
- 探索插件的实现用时约12小时，只完成了添加菜单，其他的还需更多时间，暂时不去实现

## 站点相关信息

- blog主站站 <http://wpsite.tanweiquan.cn>
- woocommerce 1站 <http://wooone.wpsite.tanweiquan.cn>
- woocommerce 2站 <http://wootwo.wpsite.tanweiquan.cn>
- woocommerce 3站 <http://woothree.wpsite.tanweiquan.cn>
- 部署文档 <http://wpsite.tanweiquan.cn/2022/04/08/wordpress%e5%ad%90%e5%9f%9f%e5%90%8d%e5%a4%9a%e7%ab%99%e7%82%b9%e9%83%a8%e7%bd%b2%e8%af%a6%e7%bb%86%e6%ad%a5%e9%aa%a4/>

主站登录账户：
```
kejiaxiaotan
wpblog2022

```

