<?php
/**
 * Plugin Name: woocommerce-101
 * Plugin URI: https://gitee.com/tanweiquan/woocommerce-101
 * Description: my first woocommerce extension example.
 * Version: 1.0.0
 * Author: TanWeiQuan
 * Author URI: https://gitee.com/tanweiquan/
 * Developer: Your Name
 * Developer URI: https://gitee.com/tanweiquan/
 * Text Domain: woocommerce-101
 * Domain Path: /languages
 *
 * Woo: 12345:342928dfsfhsf8429842374wdf4234sfd
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */ 
 
defined( 'ABSPATH' ) || exit;
if ( ! class_exists( 'My_Extension' ) ) :
    /**
     * My Extension core class
     */
    class My_Extension {
 
        /**
         * The single instance of the class.
         */
        protected static $_instance = null;
 
        /**
         * Constructor.
         */
        protected function __construct() {
            //$this->includes();
            $this->init();
        }
 
        /**
         * Main Extension Instance.
         */
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
 
        /**
         * Cloning is forbidden.
         */
        public function __clone() {
            // Override this PHP function to prevent unwanted copies of your instance.
            //   Implement your own error or use `wc_doing_it_wrong()`
        }
 
        /**
         * Unserializing instances of this class is forbidden.
         */
        public function __wakeup() {
            // Override this PHP function to prevent unwanted copies of your instance.
            //   Implement your own error or use `wc_doing_it_wrong()`
        }
 
        /**
        * Function for loading dependencies.
        */
        private function includes() {
            $loader = include_once dirname( __FILE__ ) . '/' . 'vendor/autoload.php';
 
            if ( ! $loader ) {
                throw new Exception( 'vendor/autoload.php missing please run `composer install`' );
            }
 
            require_once dirname( __FILE__ ) . '/' . 'includes/my-extension-functions.php';
        }
 
        /**
         * Function for getting everything set up and ready to run.
         */
        private function init() {
            add_action( 'admin_menu', [ $this,'register_menu_items'] );
            // Examples include: 
 
            // Set up cache management.
            // new My_Extension_Cache();
 
            // Initialize REST API.
            // new My_Extension_REST_API();
 
            // Set up email management.
            // new My_Extension_Email_Manager();
 
            // Register with some-action hook
            // add_action('some-action', 'my-extension-function');
        }
        
        /**
         * 添加菜单
         * add plugin menu for WordPress
         */
        public function register_menu_items() {
            /*add_menu_page(
                'example woo',
                'example woo',
                'manage_woocommerce',
                'example-woo', 
                [ $this, 'render' ],
                '',
                1
            );*/
            add_submenu_page( 
                'woocommerce', 
                __( 'Custom Menu', 'Custom-Menu' ), 
                __( 'Custom Menu', 'Custom-Menu' ), 
                'manage_woocommerce', 
                "Custom-Menu", 
                [ $this, 'menu_rediret' ], 
                99 
            );
            add_submenu_page( 
                'woocommerce', 
                __( 'RESTful Key', 'RESTful-Key' ), 
                __( 'RESTful Key', 'RESTful-Key' ), 
                'manage_woocommerce', 
                "RESTful-Key", 
                [ $this, 'get_rest_key' ], 
                99 
            );
            // Register a standalone menu item. 
            /*Menu::add_plugin_item(
                array(
                    'id'         => 'my-extension',
                    'title'      => 'My Extension',
                    'capability' => 'manage_woocommerce',
                    'url'        => 'my-extension-slug',
                )
            );*/
         
            // Register a navigation category with a child item.
            /*Menu::add_plugin_category(
                array(
                    'id'     => 'my-extension-category',
                    'title'  => 'My Extension Category',
                    'parent' => 'woocommerce',
                )
            );
         
            Menu::add_plugin_item(
                array(
                    'id'         => 'my-extension-cat-page',
                    'title'      => 'My Extension Cat Page',
                    'capability' => 'manage_woocommerce',
                    'url'        => 'my-extension',
                    'parent'     => 'woocommerce',
                )
            );*/
        }
        /**
         * Renders the rest_key page.
         * 菜单页面内容
         */
        public function get_rest_key(){
            if (!current_user_can('manage_options')) {
                return;
            }
            require_once dirname( __FILE__ ) . '/' . 'views/restful-key.php';
        }
        /**
         * Renders the Custom Menu page.
         * 菜单页面内容
         */
        public function menu_rediret() {
            //var_dump(Menu::get_mapped_menu_items());
            //add_action( 'admin_notices', array( $this, 'admin_notices' ), 15 );
            // check user capabilities
            if (!current_user_can('manage_options')) {
                return;
            }
            require_once dirname( __FILE__ ) . '/' . 'views/menu-rediret.php';
            
        }
        /**
         * 移除菜单
         * delete plugin menu for WordPress
         */
        public function remove_menu_items()
        {
            remove_menu_page([ $this, 'menu_rediret' ]);
            remove_menu_page([ $this, 'get_rest_key' ]);
        }
        
        
    }
endif;
/**
 * 启用
 * Activation hooks for WordPress
 */
function myPrefix_extension_activate() {
    
    // Your activation logic goes here.
}
register_activation_hook( __FILE__, 'myPrefix_extension_activate' );
/**
 * 创建插件实例
 * Function for delaying initialization of the extension until after WooComerce is loaded.
 */
function my_extension_initialize() {
 
    // This is also a great place to check for the existence of the WooCommerce class
    if ( ! class_exists( 'WooCommerce' ) ) {
    // You can handle this situation in a variety of ways,
    //   but adding a WordPress admin notice is often a good tactic.
        return;
    }
 
    $GLOBALS['my_extension'] = My_Extension::instance();
}
add_action( 'plugins_loaded', 'my_extension_initialize', 10 );

/**
 * 停用
 * deactivation hooks for WordPress
 */
function myPrefix_extension_deactivate() {
    // Your deactivation logic goes here.
    add_action('admin_menu', [$GLOBALS['my_extension'],'remove_menu_items'], 99);
    // Don't forget to:
    // Remove Scheduled Actions
    // Remove Notes in the Admin Inbox
    // Remove Admin Tasks
}
register_deactivation_hook( __FILE__, 'myPrefix_extension_deactivate' );